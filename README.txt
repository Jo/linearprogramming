# linear_programming
ING1 EISTI Pau - Joseph Bexiga 
Implémentation de l'algorithme du simplexe, qui consiste à trouver l'optimum d'un problème (qui doit être traduit mathématiquement sous forme de matrice, cf "problem1_1_1.txt")
Le problème en question permet de minimiser une fonction sur un ensemble défini par des inégalités.

Pour plus d'information sur le simplexe: 
https://fr.wikipedia.org/wiki/Algorithme_du_simplexe#Le_probl%C3%A8me_%C3%A0_r%C3%A9soudre


