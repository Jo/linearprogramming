import numpy as np
from fractions import Fraction

#Fonction qui retourne un tableau de valeur à partir d'un fichier texte formaté.
def traitementFichier(path):
    #Lecture du fichier en lecture
    f = open(path, 'r')
    #Initialisation du tableau
    tab = []

    i = 0 #Compteur pour connaître le nombre de lignes
    for line in f:
        x = line.replace("\n", "").split(" ")
        #Correspond à une ligne du tableau
        y = []
        i += 1
        for e in x:
            #Permet de récupérer toutes les valeurs en float, même celles écrites sous forme fractionnaires (ex: "1/2" => 0.5)
            elt = float(Fraction(e))
            #On ajoute chaque élément à la ligne
            y.append(elt)
        #On ajoute chaque ligne au tableau
        tab.append(y)
    #print(tab)

    # S'il y a i-1 contraintes alors on a i-1 variables d'écarts
    # => Matrice identité de taille i-1
    I = np.eye(i-1)
    I = np.array(I, float)

    contraintes = tab[1:i]
    a = np.shape(contraintes)

    #Séparation membre gauche et membre droit des contraintes
    c = []
    for l in contraintes:
        length = len(l)
        c.append(l[length-1])
        del(l[length-1])

    #Membre de droite du critère
    c.insert(0, 0)

    #Conversion du membre de droite en un vecteur colonne
    c = np.array(c)[:,np.newaxis]

    #Vecteur nul concaténé au critère. (première ligne de la matrice)
    zeros = np.zeros(i-1)
    zeros = np.array(zeros, float)[np.newaxis,:]
    I = np.concatenate((zeros, I))

    #On concatène le tableau avec la matrice identité, puis avec le critère
    tab = np.concatenate((tab, I), axis=1)
    tab = np.concatenate((tab, c), axis=1)

    return(np.array(tab, float))

#Fonction qui retourne(l'indice colonne de la variable sortante, le tableau des contraintes, ainsi que le vecteur membre de droite
def get_info(tab):
    #Au début, la variable entrante est la plus grande des variables de départ
    vCritere = list(tab[0][0:len(tab[0])-1])
    vEntrante = max(vCritere)

    #Indice colonne de la variable sortante
    k = vCritere.index(vEntrante)
    #On calcule les rapports: (coef membre de droite) / (coef de la colonne de la variable entrante)
    #Contraintes
    C = tab[1:]
    hauteur = C.shape[1]

    #Membre de droite
    b = tab[1:,hauteur-1]#.reshape(4,1)
    return(k, C, b)

def phases1(tab):
    print(tab)

def initialisation(tab):
    if(exists_vEntrante(tab)):
        k, C, b = get_info(tab)
        rapports, optimum = getRapports(tab, k)
        return(variable_sortante(tab, k))
        #return(tab[j+1, k], j+1, k, optimum)

def pivotage(tab, coef, ligne, colonne):
    #On divise la ligne correspondant à la variable sortante par le coefficient
    tab[ligne] = tab[ligne]/coef
    for i in range(0, tab.shape[0]):
        if i != ligne:
            tab[i] = tab[i]-tab[ligne]*(tab[i][colonne]/tab[ligne][colonne])

#Vérifie qu'il y a une variable positive dans le critère
def exists_vEntrante(tab):
    l = list(filter(lambda x: x>0, tab[0]))
    if len(l) > 0:
        res = True
    else:
        res = False
    return(res)

#Fonction qui détermine s'il existe une variable sortante
def exists_vSortante(tab, k):
    rapports = getRapports(tab, k)[0]
    l = list(filter(lambda x: x>0, rapports))
    if len(l) > 0:
        res = True
    else:
        res = False
    return(res)

def variable_entrante(tab):
    #Cas standart: on choisit la plus grande des variable positives
    if exists_vEntrante(tab):
        l = list(filter(lambda x: x>0, tab[0]))
        k = list(tab[0]).index(max(l))
    else:
        k = None
    return(k)

#Calcul des rapports: (coef membre de droite) / (coef de la colonne de la variable entrante)
#Retourne la liste des rapports avec l'indice ligne de la variable sortante
def getRapports(tab, k):

    C, b = get_info(tab)[1:3]

    #Calcul des rapports pour déterminer la variable sortante
    rapports = []
    #Initialisation du minimum des rapports à l'infini
    minimum = float("inf")

    optimum = False
    for i in range(0, len(b)):
        if C[i, k] != 0:
            r = b[i]/C[i,k]
            rapports.append(r)

    return(rapports, optimum)

def variable_sortante(tab, vEntrante):
    #k indice de la variable entrante
    #k = variable_entrante(tab)
    k = vEntrante
    rapports, optimum = getRapports(tab, k)
    l = list(filter(lambda x: x >= 0, rapports))
    minimum = min(l)
    j = rapports.index(minimum)
    return(tab[j+1, k], j+1, k, optimum)

def simplexe(path):
    tab = traitementFichier(path)
    print(path +":\n Tableau de départ: \n",tab)
    coef, ligne, colonne, opt = initialisation(tab)
    optimum = {'x'+str(colonne+1): ligne}
    borne = True
    #Tant que l'on n'a pas trouvé d'optimum et qu'il y a une variable sortante..
    while (not opt) and borne:
        pivotage(tab, coef, ligne, colonne)
        vEntrante = variable_entrante(tab)
        if not exists_vEntrante(tab):
            opt = True
        else:
            #Cas où il n'y a pas de variable sortante: problème non borné
            if not exists_vSortante(tab, vEntrante):
                borne = False
            #Cas où on trouve une variable sortante
            else:
                coef, ligne, colonne, opt = variable_sortante(tab, vEntrante)
                optimum['x'+str(colonne+1)] = ligne

    #AFFICHAGE DU RESULTAT:
    if borne:
        vOptimum = -1*tab[0,len(tab[0])-1]
        indiceX1 = optimum['x1']
        indiceX2 = optimum['x2']
        print("\tLa valeur de l'optimum est : ", vOptimum)
        print("\t", tab[indiceX1, len(tab[0])-1], tab[indiceX2, len(tab[0])-1], "\n")
    else:
        print("\tProblème non borné")

simplexe("problem1_1_1.txt")
simplexe("problem1_1_2.txt")
simplexe("problem1_1_3.txt")
simplexe("problem1_2.txt")
simplexe("problem1_4_a.txt")
